ToDo's and issues for code generation

1) cleanup the 'ascii' directory.
2) standardize the filenames:
    a) <class>.xlsx
    b) <class>.cvs
    c) <class>.json
    d) generate_<class>_json.dart
    e) <class>_class_table.dart
    f) generate_<class>_class.dart
    g) <class>_test.dart
    h) <class>.dart  ; the production code
    
3) In some special cases when the table needs to be modified from the DICOM table
   the following are created:
    a) generated_<class>.txt
    b) generated_<class>.xlsx
    c) generated_<class>.json
    