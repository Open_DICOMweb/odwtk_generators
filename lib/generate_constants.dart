// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library generate_constants;

//import 'dart:io';

import 'package:generators/generators.dart';

/**
 * Generate a library with constant definitions of the form
 *
 *  const $name = $value; // $comment
 *
 * A generated constant library has the following format:
 *
 * $ProjectCopyrightAuthor
 * $generatedFileNotice
 * $documentation
 * $constantDefinitions
 * $endOfGeneratedFileNotice
 *
 */

class GenerateConstants {
  static const PROGRAM = 'generators/generate_contants.dart';
  static const VERSION = '0.1.1';
  ClassTable table;


  GenerateConstants(this.table);

  // Generate Constant Definitions
  String generateDefinitions() {
    String s = "  // ${table.className} Constants\n";
    Members members = table.members;
    for (List m in members) {
      String name    = m[0];
      String type    = m[1];
      String value   = m[2];
      String comment = m[3];
      //TODO make the string align in columns
      s += 'const $type $name = const $value\t // $comment;';
    }
    return s;
  }

  generateSourceString() {
    String program = 'generate_constants.dart';
    String libraryName = '${table.className}_constants;';
    String s = "";
    s += projectCopyrightAuthor;
    s += 'library $libraryName\n';
    s += beginGeneratedFileNotice(program, VERSION);
    s += generateDefinitions();
    s += endGeneratedFileNotice(program);
  }

}