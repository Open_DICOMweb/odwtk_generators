// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library error;
// Part of the Generator package

/**
 * The Generators parser has arrived at an inconsistent state and cannot proceed.
 *
 * This is a generic error used for a variety of different erroneous
 * actions. The message should be descriptive.
 */
class ParseError extends Error {
  final String prefix = "Parse Error: ";
  final String message;

  ParseError([this.message = "???"]);

  String toString() => prefix + message;
}

void parseError(String message) {
  throw new ParseError(message);
}

class GeneratorError extends Error {
  final errorPrefix = "Generator error: ";
  final message;

  GeneratorError([this.message = "???"]);

  String toString() => errorPrefix + message;
}

//TODO replace general errors with more specific ones for version 0.2.0
/**
 * General error message
 *
 */
void error(message) {
  throw new GeneratorError(message);
}


