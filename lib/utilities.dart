// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

// package: generators
library utilities;

//import 'package:generators/generators.dart';

// This function assumes that all the inner lists are the same length empty elements
// should have "" or null.
List getMaxWidths(List<List> lists) {
  int length = lists[0].length;
  List maxWidths = new List(length);
  maxWidths.fillRange(0, length, 0);
  for (List list in lists) {
    assert(list.length == length);
    for (int i = 0; i < length; i++) {
      var v = list[i];
      String s = (v is String) ? v : v.toStrin();
      int len = s.length;
      if (maxWidths[i] < len) maxWidths[i] = len;
    }
  }
  return maxWidths;
}
