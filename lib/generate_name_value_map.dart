// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library generate_name_value_map;

import 'dart:collection';
//import 'dart:io';

import 'package:generators/generators.dart';

/**
 * Generate a [Library Part] containing a [map] from [Name] to [Value].
 *
 */
class GenerateNameValueMap extends Maps {
  static const PROGRAM = 'generators/generate_name_to_value_map.dart';
  static const VERSION = '0.1.1';
  ClassTable table;

  // Constructor
  GenerateNameValueMap(this.table) {
    checkTable(table);
  }

  // Generate Constant Map Definition
  generateNameValueMap() {
    String s = "  // ${table.className} Name to Value Map\n";
    s += 'const Map<String, ${table.memberValueType}> lookup = const Map {\n';
    List<List> members = table.members;
    for (List m in members) {
      String name    = m[0];
      String type    = m[1];
      String value   = m[2];
      String comment = m[3];
      s += '    $name:  const $value\t // $comment;';
    }
    return s;
  }

  generateSourceString() {

  }
}