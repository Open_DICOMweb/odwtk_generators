// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library file_type;

class FileType {
  //final String name;
  final String type;
  final String suffix;
  final String library;

  const FileType( this.type, this.suffix, this.library);

  static const Class        = const FileType("Class",        "_class",     "library");
  static const Constants    = const FileType("Constants",    "_constants", "library");
  static const NameValueMap = const FileType("NameValueMap", "_name_value_map", "part of");
  static const ValueNameMap = const FileType("ValueNameMap", "_value_name_map", "part of");

  String filename(String className)      => className + this.suffix;
  String dartFilename(String className)  => filename(className) + '.dart';
  String libraryString(String className) => this.library + ' $className;';

}


