// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:more/collection.dart';
import 'package:unittest/unittest.dart';

//import 'package:utilities/utilities.dart';


void main() {

  bool testBitInSet(BitList charset, int offset) {
    return charset[offset] == true;
  }

  bool testBitNotInSet(BitList charset, int offset) {
    return charset[offset] == false;
  }

  bool testBitRangeInSet(BitList charset, start, end) {
    for(var i = start; i < end; i++) {
      if (charset[i] == false) throw new Error();
    }
    return true;
  }

  bool testBitRangeNotInSet(BitList charset, start, end) {
    for(var i = start; i < end; i++) {
      if (charset[i] == true) throw new Error();
    }
    return true;
  }

  bool testDigits(BitList charset) {
    return testBitRangeInSet(charset, Ascii.DIGIT_0, Ascii.DIGIT_9+1);
  }

  group('ascii_constants', () {

    test('AETitle', () {
      Ascii charset = Ascii.AETITLE;

      // SPACE to TILDE - [BACKSLASH]
      testBitRangeNotInSet(charset, 0, Ascii.SPACE);
      testBitRangeInSet(charset, Ascii.SPACE, Ascii.BACKSLASH);
      testBitNotInSet(charset, Ascii.BACKSLASH);
      testBitRangeInSet(charset, Ascii.BACKSLASH+1, Ascii.TILDE+1);
      testBitNotInSet(charset, 127);
    });

    solo_test('Age', () {
      Ascii charset = Ascii.AETITLE;

      // SPACE to TILDE - [BACKSLASH]
      testBitRangeNotInSet(charset, 0, Ascii.DIGIT_0);
      testDigits(charset);
      testBitRangeNotInSet(charset, Ascii.DIGIT_9+1, Ascii.LETTER_D);
      testBitInSet(charset, Ascii.LETTER_D);
      testBitRangeNotInSet(charset, Ascii.LETTER_D+1 , Ascii.LETTER_M);
      testBitInSet(charset, Ascii.LETTER_M);
      testBitRangeNotInSet(charset, Ascii.LETTER_M+1 , Ascii.LETTER_W);
      testBitInSet(charset, Ascii.LETTER_W);
      testBitRangeNotInSet(charset, Ascii.LETTER_W+1 , Ascii.LETTER_Y);
      testBitInSet(charset, Ascii.LETTER_Y);
      testBitRangeInSet(charset, Ascii.LETTER_Y+1, 127);
    });

  });
}