// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library class_table_json_template;

import 'package:generators/generators.dart';

String classTableTemplate(ClassTable table) => """
{ 'thisFilename': ${table.thisFilename},
  'JsonType':     'ClassTable',
  'info':         'This file contains the JSON ClassTable template for ${table.className}',
  'className':    "",
  'version':      "",
  'description':  "",
  'docLink':      "",
  'packagePath':  "",
  'extensions':   [""],
  'interfaces':   [""],
  'fields':       {},
  'members':      [[]],
  'methods':      ["";

""";

String fieldsTemplate = """
{ 'modifier': "",
  'type': [],
  'name':     [],
  'value':    [],
  'args':     [[]]
}
""";

String memberTemplate = """
  'modifier': "",
  'template': [],
  'members':  [[]],
""";


String classTableTemplateHelp = """
{ 'thisFilename': 'The name of the JSON file containing this class',
  'JsonType':     'A Pseudo Type for this table',
  'info':         'A String explaining what the file is used for',
  'className':    'The name of the class',
  'version':      'The version number of this class',
  'description':  'Documentation for this class',
  'docLink':      'The URI of documentation for this class',
  'packagePath':  'The path for the package in which to generate the source',
  'extends':      'A String naming the class which this class extends', 
  'interfaces':   'A List<String> of interfaces implemented',
  'fields':       'A Map of the fields of this class (See fieldTemplate)',
  'members':      'A Map of the members of this class',
  'methods':      'A List<String> containing method definitions'
}
""";