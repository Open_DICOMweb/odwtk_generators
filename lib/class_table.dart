// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library class_table;

import 'dart:convert';
import 'dart:io';
import 'package:generators/generators.dart';

/**
 * This class contains the information necessary to generate the libraries for a
 * base class.
 *
 * The format is designed to generate the following libraries:
 *    $className_constants.dart,   - a set of top level named constants
 *    $className_lookupName.dart,  - a map where map[name] -> value
 *    $className_lookupValue.dart, - a list or map, where foo[value] -> [name]
 *    $className.dart              - a const class with full definition
 *
 * The JSON file defining a class should have a filename in library name format,
 * where the filename is similar to the class name.  For example, if the class
 * is named 'ClassTable', then the filename should be named 'class_table.json'.
 *
 */
class ClassTable {
  static const docLinkRoot = "doc/";
  String                  thisFilename; // The path to JSON file containing this class
  String                  info;           // A brief description of this class
  String                  jsonType = "ClassTable";   // A Pseudo Type for this table
  String                  className;    // The name of the class
  String                  version;      // The version number of this class
  Map<String, String> description;  // Documentation for the class
  String                  docLink;      // Uri to documentation
  String                  packagePath;  // The path to the package for the generated source
  String                  superClass;   // extends xxx implements yyy..
  List<String>          interfaces;   // implements these interfaces
  Fields                 fields;       // A list of field defs like [name, type, value, comment]
  Members               members;      // A list of member defs: [name, arg1, arg2...]
  List<String>          methods;      // A String containing method definitions

  /*
  ClassTable(this.mapType, this.className, this.extentions, this.interfaces,
             this.outputFilename, this.libraryType, this.libraryName, this.description,
             this.version, this.docLink, this.nFields, this.fieldModifier, this.fields,
             this.members, this.methods, this.membersFormat);
  */


  String libraryString(FileType fType) => fType.libraryString(className);

  //TODO flush this
 /*
  String generateProjectCopyrightAuthor() {
    DateTime dateTime = new DateTime.now();
    var s = """
// DICOMweb Toolkit in Dart
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
// *** Generated on $dateTime ***
""";
    return s;
  }
*/

  static ClassTable  read(String filename) {
    Map data = JSON.decode(new File('$filename.json').readAsStringSync());
    print(data);
    ClassTable table = new ClassTable();
    // Initialize Table
    table.thisFilename = data['filename'];
    table.info         = data['info'];
    table.className    = data['className'];
    table.version      = data['version'];
    table.description  = data['description'];
    table.docLink      = data['docLink'];
    table.superClass   = data['superClass'];
    table.interfaces   = data['interfaces'];

    Fields fields = new Fields(data['fields']);
    table.fields       = fields;
    table.members      = new Members(fields, data['members']);
    table.methods      = data['methods'];
    return table;
  }

  write(FileType fType) {
    File   output = new File(fType.dartFilename(className));
    String json   = JSON.encode(this);
    output.writeAsStringSync(json);
  }


  List listToJson(List list) {
    List out = new List();
    for (var e in list) {
      if (e is List) listToJson(e);
      else if (e is String) out.add("\"" + e + "\"");
      else if ((e is bool) || (e == null))  out.add(e);
      // For anything else it has to generate its own json object
      else out.add(e.toJson);
    }
    return out;
  }

  String toJson() {
    // only create this if JSON.encode isn't acceptable
    String json = """ 
{ \"info\": "This is a DWTK class definition file", // not used
  \"thisFilename\":  $thisFilename,
  \"className\":     $className,
  \"version\":       $version,
  \"description\":   $description,
  \"docLink\":       $docLink,
  \"superClass\":    $superClass,
  \"interfaces\":    $interfaces,
  \"fields\":        $fields,
  \"members\":       $members,
  \"methods\":       $methods,
""";
    return json;
  }

  JsonEncoder encoder = new JsonEncoder.withIndent("  ");


  Map readClassTable(String filename) {
    File file = new File(filename);
    String s = file.readAsStringSync();
    Map table = JSON.decode(s);
    if ("ClassTable" != table["JsonType"]) throw new Exception("Not Class Table");
    return table;
  }

  Map writeClassTable(Map table, String filename) {
    if ("classTable" != table["JsonType"]) throw new Exception("Not Class Table");

    File file = new File(filename);
    String s = encoder.convert(table);
    file.writeAsStringSync(s);
    Map classTable = JSON.decode(s);
    return classTable;
  }
}


