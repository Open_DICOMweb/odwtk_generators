// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library fields;

import 'dart:collection';
//import 'dart:convert';
//import 'dart:io';
//import 'dart:typed_data';
import 'package:generators/generators.dart';

/**
 * Fields define the fields of a ClassTable.
 *
 * Fields is a [Map<String, List], where each key/value pair has the format:
 *
 *    "name": [modifier, type, name, value, comment].
 *
 * If the field does not have a value it should contain the empty string "".
 * For example the field definition:
 *
 *    ["const", "String", "s", "This is a string", "This is a comment"]
 *
 * would generate the following string:
 *
 *    'const String s = "This is a string"; // This is a comment\n'
 */
typedef String Formatter(String name, String type, String value);

class Fields extends Maps {
  Map<String, String> fields;
  String     modifier;    // If the modifier is global
  List<int>  _maxWidths;  // The length of the largest String in each columns
  Formatter  formatter;   //

  // Operators, Getters & Setters
  // The basic map primitives to extend Maps
  Iterable<String> get keys   => fields.keys;
  Iterable<List>    get values => fields.values;
  List<int>          get maxWidths =>
      (_maxWidths != null) ? _maxWidths : getMaxWidths(fields.values);

  dynamic operator [](String key)         => fields[key];
  void      operator []=(String key, value) => fields[key] = value;

  dynamic remove(Object key)        => fields.remove(key);

  // Constructor
  Fields(this.fields, [this.formatter]) {
   // validate();
  }

  // Validator
  bool validate() {
    //TODO are the assertions good enough?
    for (List field in fields.values) {
      assert(field.length == 5);
      for (int i = 0; i < 5; i++) {
        assert(field[i] is String);
        if (maxWidths[i] < field[i].length) maxWidths[i] = field.length;
      }
    }
    return true;
  }

  String toDart([int indent = 0, String prefix = ""]) {
    String out = "";
    for (List<String> field in fields) {
      out = out.padRight(indent);
      out += prefix;
      for(int i = 0; i < 4; i++) {
        String fd  = field[0];
        int    pad = maxWidths[i] - fd.length;
      }
      out += ' ${field[3]};\n';
    }
    return out;
  }
}
