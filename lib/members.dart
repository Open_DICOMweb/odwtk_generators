// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library members;

import 'package:generators/generators.dart';

/**
 * Members define the members of a ClassTable.
 *
 * When written to JSON Members is a List<List>, where the inner List defines
 * a Member, which is of the form [name, value, @args], where @args are other
 * fields for the constructor.
 */
class Members {
  static const MODIFIER = 0;
  static const TYPE     = 1;
  static const NAME     = 2;
  static const VALUE    = 3;
  static const COMMENT  = 4;

  Fields     fields;
  List       template;
  List<List> members;
  List<int>  maxWidths;  // The length of the largest String in each columns
  Formatter  formatter;  //

  Members(this.fields, this.members, [this.formatter]) {
    //validate();
  }

  bool validate() {
    for (List m in members) {
      //TODO why can't we use field.length?
      assert(m.length == fields.keys.length);
      for (int i = 0; i < m.length; i++) {
        int len = m[i].toString().length;
        if (maxWidths[i] < len) maxWidths[i] = len;
        //TODO check field types here
      }
    }
    return true;
  }

  String generateConstants() {
    String s = "";
    for (List m in members) {
      String type    = m[TYPE];
      String name    = m[NAME];
      String value   = m[VALUE];
      String comment = m[COMMENT];
      s += 'const $type $name = $value; //$comment';
    }
    return s;
  }

  // Internal Helpers
  String _getLine(List m, var formatter, [int indent = 0, bool comments = true]) {
    String out     = "".padRight(indent);
    String name    = _paddedString(m, NAME);
    String value   = _paddedString(m, VALUE);
    String comment = (comments) ? '// ${_paddedString(m, COMMENT)}' : "";
    return out += formatter(name, value, comment);
    }

  String _paddedString(List m, int i) {
    String s = m[i];
    int    pad = maxWidths[i] - s.length;
    return s.padRight(pad);
  }
  /*
  String generateNameValueMap(String className, bool comments) {
    String s = "Map ${className}NameValueMap = { ";
    for (List m in members) {
      String name    = paddedString(m, NAME);
      String value   = paddedString(m, VALUE);
      String comment = paddedString(m, COMMENT);
      comment = (comments) ? '//$comment' : "";
      s += '$name: $value, $comment\n';
    }
    s += s.substring(0, s.length - 1);
    s += '};\n';
    return s;
  }
  */

  String _fmtNameValue(String name, String value, String comment) =>
    '$name: $value, $comment\n';

  String generateNameValueMap(String className, [int indent = 0, bool addComment = true]) {
    String out = "Map ${className}NameValueMap = { ";
    for (List m in members) out += _getLine(m, _fmtNameValue, indent, addComment);
    // Remove last comma & newline
    return out += out.substring(0, out.length - 2) + ' };\n';
  }


  String _fmtValueName(String name, String value, String comment) =>
    '$value: $name, $comment\n';

  String generateValueNameMap(String className, [int indent = 0, bool addComment = true]) {
    String out = "Map ${className}ValueNameMap = { ";
    for (List m in members) out += _getLine(m, _fmtValueName, indent, addComment);
    // Remove last comma & newline
    return out += out.substring(0, out.length - 2) + ' };\n';
  }

  /*
  String generateValueNameMap(String className, bool comments) {
    String s = "Map ${className}NameValueMap = { ";
    for (List m in members) {
      String name    = paddedString(m, NAME);
      String value   = paddedString(m, VALUE);
      String comment = paddedString(m, COMMENT);
      comment = (comments) ? '//$comment' : "";
      s += '$value: $name, $comment\n';
    }
    s += s.substring(0, s.length - 1);
    s += '};\n';
    return s;
  }
  */

  String _fmtLookkupList(String name, String value, String comment) =>
    '$name, $comment\n';

  String generateLookupList(String className, [int indent = 0, bool addComment = true]) {
    String out = "Map ${className}ValueNameMap = { ";
    for (List m in members) out += _getLine(m, _fmtValueName, indent, addComment);
    // Remove last comma & newline
    return out += out.substring(0, out.length - 2) + ' ];\n';
  }

  /*
  String generateLookupList(String className, [int indent = 0, bool comments = true]) {
    String out = "Map ${className}Lookup = [ ";
    for (List m in members) {
      String name    = paddedString(m, NAME);
      String comment = paddedString(m, COMMENT);
      comment = (comments) ? '//$comment' : "";
      out += '$name, $comment\n';
    }
    out += out.substring(0, out.length - 2);
    out += '];\n';
    return out;
  }
  */
  // format of member is [name, value, @fields]
  // '  static const $name = const Class(@fields);'

/*
  // format of member is [name, value, @fields]
  // '  static const $name = const Class(@fields);'
  String toDart(int indent) {
    String out = "";
    for (List<String> field in fields) {
      out = out.padRight(indent);
      for(int i = 0; i < 3; i++) {
        String fd  = field[0];
        int    pad = maxWidths[i] - fd.length;
      }
      out += ' ${field[3]};\n';
    }
    return out;
  }
*/

}